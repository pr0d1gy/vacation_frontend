'use strict';

angular
    .module('vacationApp', [
        'ngAnimate',
        'ngCookies',
        'ngDialog',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.bootstrap',
        'infinite-scroll',
        'services.config',
        'angular-growl',
        'datePicker'

    ])
    .config(function ($routeProvider, $httpProvider) {

        $routeProvider
            .when('/login/', {
                templateUrl: 'views/authorization.html',
                controller: 'AuthorizationCtrl',
                title: 'Login'
            })
            .when('/singup/', {
                templateUrl: 'views/singup.html',
                controller: 'SingupCtrl',
                title: 'Singup'
            })
            .when('/user/', {
                templateUrl: 'views/user/main.html',
                controller: 'UserMainCtrl',
                title: 'User Main'
            })
            .when('/user/newVacation/', {
                templateUrl: 'views/user/new_vacation.html',
                controller: 'NewVacationCtrl',
                title: 'User New Vacation'
            })
            .when('/profile/edit/', {
                templateUrl: 'views/user/edit_profile.html',
                controller: 'UserProfileCtrl',
                title: 'Edit Profile'
            })
            .when('/user/history/', {
                templateUrl: 'views/user/history.html',
                controller: 'UserHistoryCtrl',
                title: 'User History'
            })
            .when('/admin/', {
                templateUrl: 'views/admin/main_admin.html',
                controller: 'CalendarCtrl',
                title: 'Admin'
            })
            .when('/admin/settings/', {
                templateUrl: 'views/admin/settings.html',
                controller: 'SettingsCtrl',
                title: 'Admin Settings'
            })
            .when('/admin/list/', {
                templateUrl: 'views/admin/list.html',
                controller: 'ListCtrl',
                title: 'Admin List'
            })
            .when('/admin/vacation/', {
                templateUrl: 'views/admin/new_vacations.html',
                controller: 'NewVacationCtrl',
                title: 'Admin New Vacation'
            })
            .when('/admin/edit/', {
                templateUrl: 'views/admin/edit_profile.html',
                controller: 'UserProfileCtrl',
                title: 'Edit Admin Profile'
            })
            .when('/manager/', {
                templateUrl: 'views/manager/main_manager.html',
                controller: 'CalendarCtrl',
                title: 'Manager'
            })
            .when('/manager/settings/', {
                templateUrl: 'views/manager/settings.html',
                controller: 'SettingsCtrl',
                title: 'Manager Settings'
            })
            .when('/manager/list/', {
                templateUrl: 'views/manager/list.html',
                controller: 'ListCtrl',
                title: 'Manager List'
            })
            .when('/manager/vacation/', {
                templateUrl: 'views/manager/new_vacations.html',
                controller: 'NewVacationCtrl',
                title: 'Manager New Vacation'
            })
            .when('/manager/edit/', {
                templateUrl: 'views/manager/edit_profile.html',
                controller: 'UserProfileCtrl',
                title: 'Edit Manager Profile'
            })
            .otherwise({
                redirectTo: '/login'
            });


        $httpProvider.defaults.withCredentials = true;
    })
    .config(['growlProvider', function (growlProvider) {
        growlProvider.globalTimeToLive(5000);
    }])
    .run(['$rootScope', 'AuthorizationService', '$location', 'configuration', '$cookies', function ($rootScope, AuthorizationService, $location, configuration, $cookies) {
        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            //Change page title, based on Route information
            if (current.hasOwnProperty('$$route')) {
                var title = current.$$route.title;
                if (current.$$route.title == 'ConfigurationUsername') {
                    title = 'Configuration for ' + AuthorizationService.userName
                }
                $rootScope.title = title;
            }
        });

        $rootScope.$on('$routeChangeStart', function (event, nextRoute, routeParams) {
            if ($cookies.get('token') && localStorage.getItem('user') && !AuthorizationService.isLoggedIn()) {
                AuthorizationService.initSession($cookies.get('token'));
            } else {
                if (!AuthorizationService.isLoggedIn()) {
                    $location.href = '/login/';
                } else {

                }
            }
            var user = JSON.parse(localStorage.getItem('user'));
            if (user) {
                var count = 0;
                if (user.group_code =='2' || user.group_code == '3' ) {
                    var  $admin_count = $cookies.get('loggedInAdminCount');
                    ($admin_count) ? $cookies.put('loggedInAdminCount',(parseInt($admin_count)+1)):$cookies.put('loggedInAdminCount',count);

                    if (!$admin_count > 2 && user.group_code=='2') {
                        $location.path('/manager/');
                    }
                } else if(!$admin_count > 2 && user.group_code=='3') {
                    $location.path('/admin/');
                } else if (user.group_code =='1'){
                    var  $user_count = $cookies.get('loggedInUser');
                    ($user_count) ? $cookies.put('loggedInUser',(parseInt($user_count)+1)):$cookies.put('loggedInUser',count);

                    if (!$user_count > 2 && user.group_code=='1') {
                        $location.path('/user/');
                    }
                }

            }

        });

    }]);