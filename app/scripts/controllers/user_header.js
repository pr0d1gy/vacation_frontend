'use strict';

angular.module('vacationApp').controller('UserHeaderCtrl', ['$scope', '$location', '$routeParams', 'UsersService', '$cookies', 'AuthorizationService', function ($scope, $location, $routeParams, UsersService, $cookies, AuthorizationService) {

    if ($cookies.get('token')) {
        var user = JSON.parse(localStorage.getItem('user'));
 
        UsersService.getCurrentUser(user.id).then(function(promised){
            $scope.user = promised.data;
        });
    } else {
        $location.path('#/login/');
    }

    $scope.logout = function () {

        AuthorizationService.logout();

    }

}]);