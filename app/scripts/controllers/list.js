'use strict';

angular.module( 'vacationApp' ).controller(
    'ListCtrl', [
      '$scope',
      '$rootScope',
      'MailService',
      'VacationService',

      function ( $scope, $rootScope, MailService, VacationService ) {

        $scope.vacations = null;
        $scope.prev = '';
        $scope.next = '';

        // those are for table sort
        $scope.reverse = true;
        $scope.propertyName = 'name';
        $scope.sortBy = function(prop){
            $scope.propertyName = prop;
            $scope.reverse = !$scope.reverse;
        };


        VacationService.getVacations()
            .success(function( data ){
              
              $scope.vacations = data;

              $scope.prev = (data.previous != null) ? data.previous : '';
              $scope.next = (data.next != null) ? data.next : '';
              $scope.loaded = true;

              VacationService.setDateDifference(data);
            })
            .error(function( err ){
              throw new Error( err );
            }
        );

        $scope.getSortIcon = function(prop){
            return $scope.propertyName == prop ? ($scope.reverse ? 'fa-sort-amount-desc' : 'fa-sort-amount-asc') : 'fa-sort';
        };


        $scope.stateToText = function (num){
          var states = {
              1: 'Рассматривается',
              20: 'Одобрена',
              21: 'Отклонена',
              30: 'Подтверждена',
              31: 'Отказ'
          };

          return states[num];
        };
          
         $scope.page = function ( type ) {
          $scope.$emit( 'page' );
          if ( type == 'next' ) {
            VacationService.page( $scope.next ).success(
                function ( data, status ) {
                  $scope.vacations = data;
                  $scope.prev = (data.previous != null) ? data.previous : '';
                  $scope.next = (data.next != null) ? data.next : '';
                }
            ).error(
                function ( err ) {
                  throw err;
                }
            );
          } else if ( type == 'prev' ) {
            VacationService.page( $scope.prev ).success(
                function ( data, status ) {
                  $scope.vacations = data;
                  $scope.prev = (data.previous != null) ? data.previous : '';
                  $scope.next = (data.next != null) ? data.next : '';
                }
            ).error(
                function ( err ) {
                  throw err;
                }
            );
          }
          return false;
        }

      }
    ]
);