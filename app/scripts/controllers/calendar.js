'use strict';

angular.module( 'vacationApp' ).controller(
    'CalendarCtrl', [
      '$scope',
      '$rootScope',
      'ngDialog',
      'CalendarService',
      'VacationService',
      'UsersService',

      function ( $scope, $rootScope, ngDialog, CalendarService, VacationService, UsersService ) {
        $rootScope.$on(
            '$routeChangeStart', function ( event, currRoute, prevRoute ) {
              $rootScope.animation = currRoute.animation;
            }
        );

        $scope.employeeFilter = '';
        $scope.customEmployeeFilter = function(x){

          x = x[0].user;

          var cond1, cond2, cond3, v, s;

          s = $scope.employeeFilter.toLocaleLowerCase();

          for (var key in x) {
            v = ('' + x[key]).toLowerCase();

            cond1 = v.indexOf(s) > -1;
            cond2 = ('' + key).indexOf('$$') > -1;
            cond3 = v.indexOf('null') > -1;

            if (cond1 && !cond2 && !cond3) {
              // console.log(cond1, cond1 && !cond2, v);
              return true;
            }
          }

        };


        $scope.vacationOldEnough = function(vac){

          var
            enoughDays = 3,
            enoughDiff = enoughDays * 1000 * 3600,
            currentDay = +Date.now(),
            vacEnd = new Date(vac.date_end).getTime(),
            actualDiff = currentDay - vacEnd,
            vacState = vac.state,
            isOldEnough = ( vacState == 31 && enoughDiff < actualDiff );

          return !isOldEnough;

        };



        $scope.loaded = false;

        $scope.baseConfig = CalendarService;
        $scope.vacations = null;
        $scope.vacationsByUser = null;
        $scope.userHistory = null;
        $scope.vac = null;
        $scope.usersList = {};
        $scope.userIds = [];

        $scope.comment = false;
        $scope.commented = false;

        $scope.monthHider = {
          startMonth: '1',
          endMonth: '12'
        };

        $scope.currentYear = (new Date()).getFullYear();
        $scope.currentMonth = (new Date()).getMonth() + 1;

        $scope.selectedYear = $scope.currentYear;
        $scope.selectedMonth = $scope.currentMonth;

        $scope.yearArray = function ( min, max ) {
          $scope.years = [];
          for ( var i = $scope.currentYear - min; i <= $scope.currentYear + max; i ++ ) {
            $scope.years.push( i );
          }
        };

        $scope.yearArray( 2, 1 );

        $scope.isEmpty = function ( obj ) {
          for ( var i in obj ) {
            if ( obj.hasOwnProperty( i ) ) {
              return false;
            }
          }
          return true;
        };

        function hideMonth() {
          $scope.showMonth = [];

          if ( + $scope.monthHider.startMonth < + $scope.monthHider.endMonth ) {
            for ( var i = + $scope.monthHider.startMonth; i <= + $scope.monthHider.endMonth; i ++ ) {
              $scope.showMonth.push( '' + i );
            }
          }
          else if ( + $scope.monthHider.startMonth == + $scope.monthHider.endMonth ) {
            for ( var i = + $scope.monthHider.startMonth; i <= + $scope.monthHider.endMonth; i ++ ) {
              $scope.showMonth.push( '' + i );
            }
          }
          else {
            $scope.monthHider.startMonth = '1';
            $scope.monthHider.endMonth = '12';
          }
          $( ".swipe-area" ).animate( {scrollLeft: 0}, "slow" );

        }

        $scope.getMonthPosition = function ( month ) {
          if ( ~ ($scope.showMonth.indexOf( month )) ) {
            return ($scope.showMonth.indexOf( month ) + 1);
          } else {
            return;
          }
        };

        $scope.changeYear = function () {
          $scope.getMonthNumber( $scope.selectedMonth );
        };

        $scope.getMonthNumber = function ( month ) {

          $scope.usersList = [];
          $scope.selectedMonth = month;


          VacationService.getUserByMonth( month, $scope.selectedYear )
              .success(
              function ( data ) {
                $scope.usersByMonth = data;
                angular.forEach(
                    $scope.usersByMonth, function ( user ) {
                      $scope.usersList[user.user.id] = angular.fromJson( user.user );
                    }
                );

                diffByUsers( $scope.usersByMonth );
              }
          )
              .error(
              function ( err ) {
                throw new Error( err );
              }
          );
          //debugger;

          month = parseInt( month );

          $scope.selectMonth = [];
          $scope.prev = month - 1;

          if ( $scope.prev == 0 ) {
            $scope.prev = 12;
          }

          $scope.next = month + 1;

          if ( $scope.next == 13 ) {
            $scope.next = 1;
          }
          $scope.selectMonth.push( $scope.prev, month, $scope.next );
        };

        $scope.getMonthNumber( (new Date()).getMonth() + 1 );

        $scope.$watch(
            'monthHider', function () {
              hideMonth();
            }, true
        );

        var rank_list = [
          'Разработчик',
          'Менеджер',
          'Начальник'
        ];

        var run = function () {
          VacationService.getVacations().success(
              function ( data ) {
                $scope.vacations = data;
                //
                //angular.forEach($scope.vacations, function (vac) {
                //    $scope.usersList[vac.user.id] = angular.fromJson(vac.user);
                //
                //});

                //diffByUsers($scope.vacations);

                $scope.loaded = true;

                //$(".swipe-area").each(function(){
                //    $(this).find('.month-wrapper').find('.scrolls').each(function(){
                //        $(this).css('width',($(this).width()+'px'));
                //    });
                //    $(this).find('.month-wrapper').find('.ranges-wrapper').each(function(){
                //        $(this).css('width',($(this).width()+'px'));
                //    });
                //});

              }
          )
              .error(
              function ( err ) {
                throw new Error( err );
              }
          );
        };

        run();

        function diffByUsers( vacations ) {
          $scope.vacationsByUser = {};
          var tempMonth = [];

          vacations.forEach(
              function ( vac ) {
                if ( $scope.vacationsByUser[vac.user.id] === undefined ) {
                  $scope.vacationsByUser[vac.user.id] = [];
                }
              }
          );


          for ( var user in $scope.vacationsByUser ) {
            $scope.userIds.push( user );
            for ( var i = 1; i <= 12; i ++ ) {
              $scope.vacationsByUser[user][i] = [];
              vacations.forEach(
                  function ( vac ) {
                    if ( user == vac.user.id ) {

                      if ( vac.state != 1 ) {

                        $scope.vacationsByUser[user][i].push( angular.fromJson( vac ) );

                      } else {
                        $scope.vacationsByUser[user][i].push( angular.fromJson( vac ) );

                      }

                    }
                  }
              );

            }
          }

        }

        $scope.show_range = function ( start, month ) {
          var startDate = new Date( start );
          var month_date = ((startDate.getMonth() + 1) < 10) ? '0' + (startDate.getMonth() + 1).toString() :
              (startDate.getMonth() + 1).toString();
          var month_check = (month < 10) ? '0' + month : month;

          if ( month_check == month_date ) {
            return true;
          } else {
            return false;
          }
        };

        $scope.openVacationParameters = function ( vac ) {
          $scope.vac = null;
          var access = null;
          var user = JSON.parse( localStorage.getItem( 'user' ) );
          access = user.group_code;

          VacationService.getVacation( vac.id )
              .success(

              function ( data ) {
                $scope.vac = data;

                if ( access === 2 ) {
                  ngDialog.open(
                      {
                        template: './views/manager/manager_vacation.html',
                        className: 'ngdialog-theme-default',
                        scope: $scope
                      }
                  );

                } else if ( access === 3 ) {
                  VacationService.setDateDifference($scope.vac);

                  ngDialog.open(
                      {
                        template: './views/admin/vacation.html',
                        className: 'ngdialog-theme-default',
                        scope: $scope
                      }
                  );
                }

                VacationService.setDateDifference($scope.vac);

              }
          )
              .error(
              function ( err ) {
                throw err;
              }
          );


        };

        $scope.defineRangeFromData = function ( days, month, year ) {
          return VacationService.defineRangeFromData( days, month, year );
        };

        $scope.showUserHistory = function ( user_id ) {
          $scope.userHistory = null;

          VacationService.getVacationsByUser( user_id )
              .success(
              function ( data, status ) {

                $scope.userHistory = data;

                UsersService.getUser( user_id )
                    .success(
                    function ( user, status ) {
                      $scope.userHistoryName =
                          (user.first_name && user.last_name) ? user.first_name + ' ' + user.last_name : user.email;
                    }
                )
                    .error(
                    function ( err ) {
                      throw new Error( err );
                    }
                );

                VacationService.setDateDifference($scope.userHistory);

                ngDialog.open(
                    {
                      template: './views/user/user_history.html',
                      className: 'ngdialog-theme-default',
                      scope: $scope
                    }
                );
              }
          ).
              error(
              function ( error, status ) {
                throw new Error( error );
              }
          );
        };

        $scope.changeState = function ( id, setState, cb, comment ) {
          VacationService.changeState( id, setState, comment )
              .success(
              function ( data, status ) {

                setTimeout( cb, 300 );
              }
          )
              .error(
              function ( err ) {
                throw new Error( err );
              }
          );
        };

        $scope.addComment = function ( id, comment ) {
          VacationService.addComment( id, comment )
              .success(
              function ( data, status ) {
                $scope.commented = true;
                ngDialog.open(
                    {
                      template: '<p>Коментарий отправлен!</p>',
                      plain: true
                    }
                );
              }
          )
              .error(
              function ( error ) {
                ngDialog.open(
                    {
                      template: '<p>Коментарий не отправлен!</p>',
                      plain: true
                    }
                );
                throw error;
              }
          );
        };

        $scope.showState = function ( num ) {
          var states = [];

          states[1] = 'Рассматривается';

          states[20] = 'Одобрена';
          states[21] = 'Отклонена';

          states[30] = 'Подтверждена';
          states[31] = 'Отказ';
          return states[num];
        };

        $scope.showStateClass = function ( num ) {
          var states = [];
          states[1] = 'rangeActive';
          states[20] = 'rangeActive';
          states[21] = 'rangeApproved';
          states[30] = 'rangeActive';
          states[31] = 'rangeApproved';
          return states[num];
        };

        $scope.prettyDate = function ( day, year, month ) {
          return CalendarService.getPrettyDate( day, year, month - 1 );
        };

        $scope.$on(
            'deleteUser', function ( event, data ) {
              $scope.vacations = null;
              $scope.vacationsByUser = null;
              $scope.userHistory = null;
              $scope.vac = null;

              $scope.usersList = {};
              $scope.userIds = [];

              run();
            }
        );

        $scope.rewriteComment = function (vacation) {
          console.log('vacation', vacation);

         // $scope.$apply();

          VacationService.addComment( vacation.id, vacation.comment_admin )
              .success(

              function ( data ) {
                //
                //debugger;
              }
          )
              .error(
              //

              function(error){
                //debugger;
              }
          );
        }


      }
    ]
);