'use strict';

var showState = function ( num ) {
  var states = [];

  states[1] = 'Рассматривается';

  states[20] = 'Одобрена';
  states[21] = 'Отклонена';

  states[30] = 'Подтверждена';
  states[31] = 'Отказ';
  return states[num];
};

angular.module( 'vacationApp' ).directive(
    'confirm', function () {
      return {
        restrict: 'A',
        link: function ( scope, $elm, atrs ) {
          $elm.on(
              'click', function () {
                var elm = $( 'div[data-vac="' + atrs.confirm + '"]' );
                var classes = [
                  'rangeAccepted',
                  'rangeApproved',
                  'rangeDeclined',
                  'rangeRefused',
                  'rangeActive'
                ];
                var date_start = $( elm ).attr( 'start' );
                var date_end = $( elm ).attr( 'end' );
                var range = date_start + ' - ' + date_end;
                var pop = $( elm ).data( 'bs.popover' );
                classes.forEach(
                    function ( cl ) {
                      $( elm ).removeClass( cl );
                    }
                );

                //console.log(range);

                if ( ! atrs.manager ) {
                  $( elm ).addClass( 'rangeAccepted' );
                  $( elm ).attr(
                      'data-content', '' +
                      '<strong>Период:</strong> ' + range + ' <br/> <strong>Статус: </strong> ' + showState( 30 )
                  );
                  pop.setContent();
                } else {

                  $( elm ).addClass( 'rangeApproved' );
                  $( elm ).attr(
                      'data-content', '' +
                      '<strong>Период:</strong> ' + range + ' <br/> <strong>Статус: </strong> ' + showState( 20 )
                  );
                  pop.setContent();
                }
              }
          );

        }
      };
    }
);

angular.module( 'vacationApp' ).directive(
    'refuse', function () {
      return {
        restrict: 'A',
        link: function ( scope, $elm, atrs ) {
          $elm.on(
              'click', function () {
                var elm = $( 'div[data-vac="' + atrs.refuse + '"]' );
                var classes = [
                  'rangeAccepted',
                  'rangeApproved',
                  'rangeDeclined',
                  'rangeRefused',
                  'rangeActive'
                ];
                var date_start = $( elm ).attr( 'start' );
                var date_end = $( elm ).attr( 'end' );
                var range = date_start + ' - ' + date_end;
                var pop = $( elm ).data( 'bs.popover' );
                classes.forEach(
                    function ( cl ) {
                      $( elm ).removeClass( cl );
                    }
                );

                //console.log(range);

                if ( ! atrs.manager ) {
                  $( elm ).addClass( 'rangeRefused' );

                  $( elm ).attr(
                      'data-content', '' +
                      '<strong>Период:</strong> ' + range + ' <br/> <strong>Статус: </strong> ' + showState( 31 )
                  );
                  pop.setContent();
                } else {
                  $( elm ).addClass( 'rangeDeclined' );
                  $( elm ).attr(
                      'data-content', '' +
                      '<strong>Период:</strong> ' + range + ' <br/> <strong>Статус: </strong> ' + showState( 21 )
                  );
                  pop.setContent();
                }
              }
          );

        }
      };
    }
);

angular.module( 'vacationApp' ).directive(
    'scrollOnClick', function () {
      return {
        restrict: 'A',
        link: function ( scope, $elm, atrs ) {
          $elm.on(
              'click', function () {
                //var target = $('.month-wrapper');
                //var prevPos = atrs.target - 1 < 0 ? 0 : atrs.target - 1;

                //for (var i = 0; i < (prevPos); i++) {
                //    shift += target[i].clientWidth;
                //    if (i > 3) shift += 10;
                //}
                var shift = 0;

                $( ".swipe-area" ).animate( {scrollLeft: shift}, "slow" );

                $( '.month' ).removeClass( 'active' );
                $( this ).addClass( "active" );


              }
          );
        }
      }
    }
);


angular.module( 'vacationApp' ).directive(
    'startPosition', function () {
      return {
        restrict: 'A',
        link: function ( scope, $elm, atrs ) {
          $( document ).ready(
              function () {
                setTimeout(
                    function () {
                      var target = $( '.month-wrapper' );
                      var shift = 0;


                      //for (var i = 0; i < (atrs['startPosition'] - 1); i++) {
                      //    shift += target[i].clientWidth;
                      //    if (i > 3) shift += 5;
                      //}

                      $( ".swipe-area" ).animate( {scrollLeft: shift}, "slow" );

                    }, 1000
                )

              }
          );
        }
      };
    }
);

angular.module( 'vacationApp' ).directive(
    'horizontalScroll', function () {
      return {
        restrict: 'A',
        link: function ( scope, $elm, atrs ) {
          $( ".swipe-area" ).mousewheel(
              function ( event, delta ) {
                this.scrollLeft -= (delta * 30);
                event.preventDefault();

              }
          );

        }
      };
    }
);

angular.module( 'vacationApp' ).directive(
    'rangeLine', function () {
      return {
        restrict: 'C',
        link: function ( scope, $elm, atrs ) {

          var oToday = new Date( atrs.start );
          var oDeadLineDate = new Date( atrs.end );

          var days = oDeadLineDate > oToday ? Math.ceil( (oDeadLineDate - oToday) / (1000 * 60 * 60 * 24) ) : null;
          var size = (days + 1) * 18;
          var left = 0;

          // Makes magic *|*
          if ((scope.selectMonth[0] - oToday.getMonth()) == 1) {
            left = (oToday.getDate() * 18) - 360 - 18;
          } else {
            left = (oToday.getDate() * 18) - 18;
          }


          $( $elm ).css(
              {
                'left': left + 'px',
                'min-width': size + 'px'
              }
          );
          $( $elm ).attr( 'day', days );
          var elmClass;
          var states = {
            active: "rangeActive",
            refused: "rangeRefused",
            empty: "rangeEmpty",
            accepted: "rangeAccepted",

            approved: "rangeApproved",
            declined: "rangeDeclined"
          };

          switch ( atrs.acceptionState ) {
            case '1':
              elmClass = states.active;
              break;

            case '30':
              elmClass = states.accepted;
              break;

            case '31':
              elmClass = states.refused;
              break;

            case '20':
              elmClass = states.approved;
              break;

            case '21':
              elmClass = states.declined;
              break;
          }

          $( $elm ).addClass( elmClass );
        }
      };
    }
);
angular.module( 'vacationApp' ).directive(
    'widthLoad', function () {
      return {
        restrict: 'A',
        link: function ( scope, $elm, atrs ) {
          $( document ).ready(
              function () {
//                setTimeout(function(){

//                },6000)
              }
          );


        }
      };
    }
);
angular.module( 'vacationApp' ).directive(
    'scrollMouth', function () {
      return {
        restrict: 'A',
        link: function ( scope, $elm, atrs ) {
          $( window ).scroll(
              function () {
                var sticky = $( '.scrolls' ),
                    scroll = $( window ).scrollTop();

                if ( scroll >= 180 ) {

                  sticky.addClass( 'fixed' ).css(
                      {
                        top: scroll - 161

                      }
                  );

                  $( '.month-wrapper' ).css( 'margin-top', '64px' );
                } else {
                  $( '.month-wrapper' ).css( 'margin-top', '0px' );
                  sticky.removeClass( 'fixed' );
                }

              }
          );
        }
      };
    }
);

angular.module( 'vacationApp' ).directive(
    'popoversRange', [
      'VacationService',
      'UsersService',
      function ( VacationService, UsersService ) {
        return {
          restrict: 'A',
          link: function ( scope, $elm, atrs ) {
            $( document ).ready(
                function () {
                  var options = {
                    placement: function ( tip, element ) {
                      var offset = $( element ).offset();
                      var height = $( document ).outerHeight();
                      var width = $( document ).outerWidth();
                      var vert = 0.5 * height - offset.top;
                      var vertPlacement = vert > 0 ? 'bottom' : 'top';
                      var horiz = 0.5 * width - offset.left;
                      var horizPlacement = horiz > 0 ? 'right' : 'left';
                      var placement = Math.abs( horiz ) > Math.abs( vert ) ? horizPlacement : vertPlacement;
                      return placement;
                    }
                    , trigger: "hover"
                    , html: true
                  };
                  $( $elm ).popover( options );
                }
            );

          }
        };

      }
    ]
);


angular.module( 'vacationApp' ).directive(
    'validateForm', function () {
      return {
        restrict: 'A',
        link: function ( scope, $elm, atrs ) {
          $( document ).ready(
              function () {
                $( '#' + $elm.attr( 'id' ) ).validate(
                    {
                      showErrors: function ( errorMap, errorList ) {
                        $.each(
                            this.validElements(), function ( index, element ) {
                              var $element = $( element );

                              $element.data( "title", "" )
                                  .removeClass( "error" )
                                  .tooltip( "destroy" );
                            }
                        );

                        $.each(
                            errorList, function ( index, error ) {
                              var $element = $( error.element );

                              $element.tooltip( "destroy" )
                                  .data( "title", error.message )
                                  .addClass( "error" )
                                  .tooltip( {placement: 'right'} );
                            }
                        );
                      },
                      rules: {
                        username: {
                          email: true,
                          required: true,
                          minlength: 2
                        },
                        password: {
                          required: true,
                          minlength: 4
                        }
                      }

                    }
                );

              }
          );
        }
      };
    }
);
angular.module( 'vacationApp' ).directive(
    'validateProfile', function () {
      return {
        restrict: 'A',
        link: function ( scope, $elm, atrs ) {
          $( document ).ready(
              function () {
                $( '#' + $elm.attr( 'id' ) ).validate(
                    {
                      showErrors: function ( errorMap, errorList ) {
                        $.each(
                            this.validElements(), function ( index, element ) {
                              var $element = $( element );

                              $element.data( "title", "" )
                                  .removeClass( "error" )
                                  .tooltip( "destroy" );
                            }
                        );

                        $.each(
                            errorList, function ( index, error ) {
                              var $element = $( error.element );

                              $element.tooltip( "destroy" )
                                  .data( "title", error.message )
                                  .addClass( "error" )
                                  .tooltip( {placement: 'right'} );
                            }
                        );
                      },
                      rules: {
                        newEmail: {
                          email: true,
                          required: true,
                          minlength: 6
                        },
                        newsurname: {
                          required: true,
                          minlength: 4
                        },
                        newname: {
                          required: true,
                          minlength: 4
                        }
                      },
                      messages: {
                        newEmail: {
                          email: 'Некорректно введен E-mail адрес',
                          required: "Это поле обязательно для заполнения",
                          minlength: "E-mail должен быть минимум 6 символа"
                        },
                        newsurname: {
                          required: "Это поле обязательно для заполнения",
                          minlength: "Фамилия должна быть минимум 4 символа"
                        },

                        newname: {
                          required: "Это поле обязательно для заполнения",
                          minlength: "Имя должно быть минимум 4 символа"
                        }

                      }

                    }
                );

              }
          );
        }
      };
    }
);
angular.module( 'vacationApp' ).directive(
    'validateEmail', function () {
      return {
        restrict: 'A',
        link: function ( scope, $elm, atrs ) {
          $( document ).ready(
              function () {
                $( '#' + $elm.attr( 'id' ) ).validate(
                    {
                      showErrors: function ( errorMap, errorList ) {
                        $.each(
                            this.validElements(), function ( index, element ) {
                              var $element = $( element );

                              $element.data( "title", "" )
                                  .removeClass( "error" )
                                  .tooltip( "destroy" );
                            }
                        );

                        $.each(
                            errorList, function ( index, error ) {
                              var $element = $( error.element );

                              $element.tooltip( "destroy" )
                                  .data( "title", error.message )
                                  .addClass( "error" )
                                  .tooltip( {placement: 'right'} );
                            }
                        );
                      },
                      rules: {
                        email: {
                          email: true,
                          required: true,
                          minlength: 6
                        },

                        name: {
                          required: true,
                          minlength: 4
                        }
                      },
                      messages: {
                        email: {
                          email: 'Некорректно введен E-mail адрес',
                          required: "Это поле обязательно для заполнения",
                          minlength: "E-mail должен быть минимум 6 символа"
                        },

                        name: {
                          required: "Это поле обязательно для заполнения",
                          minlength: "Имя должно быть минимум 4 символа"
                        }

                      }

                    }
                );

              }
          );
        }
      };
    }
);

angular.module( 'vacationApp' ).directive(
    'editable', function () {

      function setCaretToEnd( element ) {
        var range = document.createRange();
        var selection = window.getSelection();

        range.setStart( element.childNodes[0], $( element ).text().length );
        range.collapse( true );
        selection.removeAllRanges();
        selection.addRange( range );
      }


      return {
        restrict: 'E',
        template: '<span>{{ ngModel }}</span>',
        replace: true,
        scope: {
          ngModel: '=',
          maxLength: '=',
          successCallback: '&successCallback',
          selectCallback: '&selectCallback'
        },
        link: function ( scope, element, attr ) {
          scope.maxLength = typeof scope.maxLength === 'undefined' ? 500 : scope.maxLength;
          var DELAY = 400, clicks = 0, timer = null, oldValue;

          element.css( 'user-select', 'none' )
              .attr( 'unselectable', 'on' );

          element.on( "click", onClick )
              .on( "dblclick", onDblClick )
              .on( "blur", onBlur )
              .on( "keydown", onKeyDown )
              .bind( 'paste', onPaste )
              .on( "keypress", onKeyPress );

          function onClick( e ) {
            clicks ++;  //count clicks

            if ( e.target.hasAttribute( 'contenteditable' ) ) {
              return;
            }

            if ( clicks === 1 ) {
              timer = setTimeout(
                  function () {
                    //perform single-click action
                    scope.selectCallback();
                    clicks = 0;  //after action performed, reset counter
                  }, DELAY
              );
            } else {
              clearTimeout( timer );  //prevent single-click action
              //perform double-click action
              oldValue = $( e.target ).text();
              $( e.target ).prop( "contenteditable", true );

              setCaretToEnd( e.target );

              e.target.focus();

              $( e.target ).scrollLeft( 300 );

              clicks = 0;  //after action performed, reset counter
            }
          }

          function onDblClick( e ) {
            e.preventDefault();  //cancel system double-click event
          }

          function onBlur( e ) {
            var text = $( e.target ).text().replace( /\s+/g, " " );
            text = _.trim( text, ' ' );
            $( e.target ).text( text );
            scope.ngModel = text;
            $( e.target ).removeAttr( "contenteditable" );

            if ( ! scope.ngModel.length ) {
              onEscape( e );
            }

            $( e.target ).scrollLeft( 0 );

            scope.successCallback();
          }

          function onEscape( e ) {
            scope.ngModel = oldValue;
            $( e.target ).text( oldValue );
            $( e.target ).removeAttr( "contenteditable" );
          }

          function onKeyDown( e ) {
            switch ( e.keyCode ) {
              case 13:
                e.preventDefault();
                onBlur( e );
                break;
              case 27:
                e.preventDefault();
                onEscape( e );
                break;
            }
          }

          function onPaste( e ) {
            e.preventDefault();
            var text = e.originalEvent.clipboardData.getData( "text/plain" );

            document.execCommand( "insertText", false, text );
          }

          function onKeyPress( e ) {
            // Prevent pressing all keys exclude 'backspace'
            if ( $( e.target ).text().length >= scope.maxLength && e.keyCode !== 8 ) {
              e.preventDefault();
              return false;
            }
          }

        }

      };
    }
)